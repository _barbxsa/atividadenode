const express = require('express');
const app = express();
const port = 8000;

//habilitar servidor
app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());


//--------------- Cliente ---------------

//get cliente
app.get('/cliente',(req, res)=>{
    console.log("Acessando o recurso CLIENTE");
    let dados = req.query;
    res.send(dados.nome + " " + dados.sobrenome);
});

//Post CLient
app.use(bodyParser.json());
app.post('/cliente/post',(req, res)=>{

    let accesspost = req.headers["access"];

    if (accesspost == 1234){
        let vpostcliente = req.body;
        console.log("Nome: " + vpostcliente.nome);
        console.log("Sobrenome: " + vpostcliente.sobrenome);
        res.send("Sucesso");
    } else{
        res.send("Chave errada");
    }
});

//Delete CLient
app.use(bodyParser.json());
app.delete('/cliente/delete/:1234',(req, res)=>{

    let deleteparamcliente = req.params;

    if (deleteparamcliente == 1234){
        res.send("Sucesso");
    } else{
        res.send("Chave errada");
    }
});

//Put CLient
app.use(bodyParser.json());
app.put('/cliente/put',(req, res)=>{

    let accessput = req.headers["access"];

    if (accessput == 1234){
        let vputcliente = req.body;
        console.log("Nome: " + vputcliente.nome);
        console.log("Sobrenome: " + vputcliente.sobrenome);
        res.send("Sucesso");
    } else{
        res.send("Chave errada");
    }
});

//--------------- Funcionario ---------------

//get funcionario
app.get('/funcionario',(req, res)=>{
    console.log("Acessando o recurso FUNCIONARIO");
    let dadosf = req.query;
    res.send(dadosf.nome + " " + dadosf.sobrenome);
});

//Funcionario Client
app.use(bodyParser.json());
app.post('/funcionario/post',(req, res)=>{

    let accesspost = req.headers["access"];

    if (accesspost == 1234){
        let vpostfuncionario = req.body;
        console.log("Nome: " + vpostfuncionario.nome);
        console.log("Sobrenome: " + vpostfuncionario.sobrenome);
        res.send("Sucesso");
    } else{
        res.send("Chave errada");
    }
});

//Delete CLient
app.use(bodyParser.json());
app.delete('/funcionario/delete/:1234',(req, res)=>{

    let deleteparamfuncionario = req.params;

    if (deleteparamfuncionario == 1234){
        res.send("Sucesso");
    } else{
        res.send("Chave errada");
    }
});

//Put CLient
app.use(bodyParser.json());
app.put('/funcionario/put',(req, res)=>{

    let accessput = req.headers["access"];

    if (accessput == 1234){
        let vputfuncionario = req.body;
        console.log("Nome: " + vputfuncionario.nome);
        console.log("Sobrenome: " + vputfuncionario.sobrenome);
        res.send("Sucesso");
    } else{
        res.send("Chave errada");
    }
});